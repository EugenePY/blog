import Vue from 'vue'
import VueRouter from "vue-router"
import Home from "./views/Home"
import Articles from "./views/Articles"

Vue.use(VueRouter)


const mk_posts_path = require.context(
    // The relative path of the components folder
    "@/assets/posts/",
    // Whether or not to look in subfolders
    false,
    // The regular expression used to match base component filenames
    /^.*\.(md)$/
);

const posts_keys = [];

mk_posts_path.keys().forEach((fileName) => {
    var key = fileName.replace("./", "")
    posts_keys.push(key)
    console.log(`Find markdown post: ${key}`);
});


export default [{
        path: '/article/:fileName',
        props: true,
        component: Articles,
        beforeEnter: (to, from, next) => {
            if (!(posts_keys.includes(to.params.fileName))) {
                next('/');
            } else {
                next();
            }

        }
    },
    { path: '*', component: Home },
]