---
title: Learning Vue by building a blog.
tags: [blog, vue]
---

## Motivation

Building a front-end application always be my short hand of my programming stack. Since I am comming from much like a data science background, writing python code building web scrapper and building commandline interface, something I need to present the tool I develop, it alway be hard to show just some texts come out of screen, and convicing those people who are not programmers that this prgramming is powerful.

## Project Plan

Basicaly what I want to build is a simple blog, using markdown as input of static website, and maybe embeded some python code results.

### Compile html from Markdown-it

The works that I can think of are, how to complie the html base on __Vue__ and __Markdown__, generating html from Markdown has many implementations, but how to make __Vue__ using raw input as input? 

### Dynamic Load the Markdown static file

### Webpack load the meta data

### Layout the Page

### Nested Components: Accessing Data from Child Components

Home -> Article Summary Card -> links to Content

### Writing Vue tests.