---
title: Time Series Data Structure
tags: [data structure, time series]
---

In this post, I would like to do some summary about some data stuctre in time series, espically in Finanacial application.

In Quantiative Finance, there are a few requirements about the data. Each time series has different id which we call it symbol, each symbol is unique, each time series has a stucture like this

```python
{symbol: "EURUSD", data: { datetime.datetime(2020, 1, 1): 1.1234 }}
```

data field is a dict which key is a timestamp, and can be ordered.

### Time interval query

#### Sorted Dictionart

```
data: [ sortedset:[timestamp], dict[timestamp, [float, int]]]
```

Interval query

```sudo code
for i in sortedset:
    if i > start and i < end:
        if i > end:
            break
        yield dict[i]
```

#### Implement on Redis

Redis has build in sortedset

timeseries_obj:symbol:field score, value

performance:

1. Long time horizon series: Frequency Tree
