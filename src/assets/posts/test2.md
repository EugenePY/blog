---
title: Blogging Like a Hacker
lang: en-US
status: publish
des: testing bloging using short describtion
---

## Test2

$$
\begin{aligned}
\beta = (X'(I - \lambda)X)^{-1}X'Y
\end{aligned}
$$

```python
import scipy
import numpy as np
N, K = 10, 5
X = np.random.normal(size=(N, K))
Y = np.random.normal(size=N)
beta = np.linalg.inv(X.dot(X.T)).dot(X.T.dot(Y))
```