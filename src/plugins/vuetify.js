import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        light: {
            primary: colors.shades.white, // #E53935
            secondary: colors.grey.darken1,
            accent: colors.shades.black,
            error: colors.red.accent3,
        },
    }
});